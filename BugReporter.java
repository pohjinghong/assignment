import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;



public class BugReporter extends User
{
    public BugReporter ()
    {
        super("", "", "", "");
    }
    
    public BugReporter (String userID, String username, String userPwd, String userType)
    {
        super(userID, username, userPwd, userType);
    }
    
}