import java.io.IOException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Poh Jing Hong
 */
public class UserTest {
    
    
    BugReporter t1 = new BugReporter("Bill","0002","bill","BugReporter") ;

    @Test
    public void testvalidateLogin() throws IOException {
        
        boolean t3 = t1.validateLogin("Bill","bill");
        
        assertEquals(true,t3);
      
    }
    
}
